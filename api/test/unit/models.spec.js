"use strict";
const ace = require("@adonisjs/ace");
const Factory = use("Factory");
const { test, trait } = use("Test/Suite")("Models");
const User = use("App/Models/User");

trait("DatabaseTransactions");

test("User Model Creation", async ({ assert }) => {
  const user = await Factory.model("App/Models/User").create();
  assert.isTrue(user instanceof User);
});

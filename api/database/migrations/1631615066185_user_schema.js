"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UserSchema extends Schema {
  up() {
    this.table("users", (table) => {
      // alter table
      table.dropColumn("provide_id");
    });
    this.table("users", (table) => {
      // alter table
      table.string("provider_id", 80).nullable();
    });
  }

  down() {
    this.table("users", (table) => {
      // reverse alternations
      table.string("provide_id").nullable();
    });
  }
}

module.exports = UserSchema;

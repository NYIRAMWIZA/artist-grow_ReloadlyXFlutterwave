'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments();
      table.string("fullName", 255).notNullable();
      table.string("email", 80).notNullable().unique();
      table.string("password", 60).notNullable();
      table.string("confirm_password", 60).notNullable();
      table.string("provide_id").nullable();
      table.string("provide").nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema

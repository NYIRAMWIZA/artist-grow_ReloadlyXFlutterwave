"use strict";
/** @type {typeof import('indicative/builds/main.js')} */
const { validations, rule } = require("indicative");
/** @type {typeof import("http-status-codes")} */
const HttpStatus = require("http-status-codes");

class SignupValidator {
  get rules() {
    return {
      fullName: "required|min:2",
      email: "required|email|unique:users",
      password: "required|string|min:3|max:12",
      confirm_password: "required|string|min:3|max:12",
    };
  }
  get messages() {
    return {
      "email.required": "You must provide an email address.",
      "email.email": "Email must be valid",
      "email.unique": "This email has be taken. Please provide a unique email",
      "password.required": "Password must not be empty",
      "password.string": "Password must be a string",
      "password.min": "Password must have at least 3 characters",
      "password.max": "Password must not have more than 12 characters",
      "confirm_password.required": " Confirm Password must not be empty",
      "cconfirm_password.string": "Confirm Password must be a string",
      "confirm_password.min":
        "Confirm Password must have at least 3 characters",
      "confirm_password.max":
        "Confirm Password must not have more than 12 characters",
    };
  }
  async fails(errorMessages) {
    return this.ctx.response
      .status(HttpStatus.UNPROCESSABLE_ENTITY)
      .json(errorMessages);
  }
}

module.exports = SignupValidator;

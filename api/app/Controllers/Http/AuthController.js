"use strict";

const User = use("App/Models/User");

class AuthController {
  async redirectToProvider({ ally, params }) {
    await ally.driver(params.provider).redirect();
  }

  async callback({ params, ally, auth, response }) {
    const provider = params.provider;
    try {
      const userData = await ally.driver(params.provider).getUser();

      const authUser = await User.query()
        .where({
          provider: provider,
          provider_id: userData.getId(),
        })
        .first();
      if (!(authUser === null)) {
        await auth.loginViaId(authUser.id);
        return response.json({ message: "Please register first" });
      }

      const user = new User();
      user.name = userData.getName();
      user.username = userData.getNickname();
      user.email = userData.getEmail();
      user.provider_id = userData.getId();
      user.avatar = userData.getAvatar();
      user.provider = provider;
      user.password = userData.getAccessToken();

      await user.save();

      await auth.loginViaId(user.id);
      return response.send(user.email);
    } catch (e) {
      console.log(e);
      return response.send("Your email is not registered");
    }
  }
}
module.exports = AuthController;

"use strict";
const User = use("App/Models/User");

class SignupController {
  async register({ request, response, auth, params }) {
    const user = request.only([
      "fullName",
      "email",
      "password",
      "confirm_password",
    ]);
    try {
      //looking for user in database
      const userExists = await User.findBy("email", user.email);

      // if user exists don't save but create a new token for him/her
      if (userExists) {
        return response.status(400).send({
          message: {
            error: "This email has be taken. Please provide a unique email",
          },
        });
      } else {
        if (user.password === user.confirm_password) {
          const data = await User.create(user);
          return response.status(201).send({ data });
        } else {
          return response.send({
            message: "password and confirm_password must match",
          });
        }
      }
    } catch (err) {
      return response.status(err.status).send({ message: err.message });
    }
  }
  async login({ request, response, auth }) {
    const user = await User.query()
      .where("email", request.input("email"))
      .first();
    // console.log(user);

    if (!user) {
      return response.json({ message: "Email does not exist!Please register" });
    } else {
      //validate the user credentials and generate the token
      // const email = request.input("email");
      const password = request.input("password");
      const token = await auth.attempt(user.email, password, {
        expiresIn: "10 days",
      });
      // console.log(token);
      return response.json({
        status: "success",
        data: token,
      });
    }
  }
}

module.exports = SignupController;

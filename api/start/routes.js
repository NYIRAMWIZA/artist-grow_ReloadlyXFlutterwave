"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.group(() => {
  /**
   * User Normal Authentication
   */

  Route.post("/signup", "SignupController.register").validator("Signup");
  Route.post("/login", "SignupController.login");
}).prefix("api/v1/users");

/**
 * User social Authentication
 */
Route.get("/auth/:provider", "AuthController.redirectToProvider").as(
  "social.login"
);
Route.get("/authenticated/:provider", "AuthController.callback").as(
  "social.login.callback"
);

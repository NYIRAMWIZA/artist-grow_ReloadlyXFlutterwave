# Crowdy Funding Backend

## Project Description

The basic technology stack is:

- MySQL (remotemysql)/SQLite (database)
- Adonis (web server)
- Adonis-Vow (Adonis vow is the test runner for Adonis framework, just install and register the provider and BOOM it just works. Setup. adonis install @adonisjs/vow.)
- Node.js (run-time environment)
- Swagger for apis documentation
- Postman for apis testing
- Adonis Vow as test tunner
- Docker

## Setup

1. Install Node.js: https://nodejs.org/
2. Installing AdonisJS `npm install @adonisjs/cli`
3. Use the adonis command to install the blueprint

```bash
adonis new api --api-only
```

or manually clone the repo and then run `npm install`.

```
cd api, And start our server  `adonis serve --dev`.
```

4. Setting up our database `npm i --save mysql` and `npm i --save sqlite3` for testing.
5. Head to our file .env (in the root of our project) to set the variables to correctly connect to our database:

```
DB_CONNECTION = mysql
DB_HOST = remotemysql
DB_PORT = YOUR_PORT
DB_USER = YOUR_DB_USER
DB_PASSWORD = YOUR_DB_PASSWORD
DB_DATABASE= YOUR_DATABASE_NAME
```

6. `$ adonis serve --dev` # start server. Done
7. `$ npm outdated` # check for outdated packages
8. `$ npm update` # update packages

### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

### Seeding

1. `$ adonis make:seed User Quiz Question` # seed the database with users,quizzes and questions
2. `$adonis seed` # creates dummy data inside database

### Testing

1. Run the following command: `adonis install @adonisjs/vow`
2. `$ adonis test` # Run tests

## Routes and Resources

| URL                  | HTTP verb | Result                        | Auth |
| -------------------- | --------- | ----------------------------- | ---- |
| /api/v1/users/signup | POST      | return a new user             |
| /api/v1/users/login  | POST      | returns the logged user token |
